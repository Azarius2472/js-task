// "use strict";
function buttonClick() {
  if(document.getElementById('x1').value === '' ||
    document.getElementById('x2').value === ''){
      alert('Поля x1 и x2 должны быть заполнены.');
    } else {
       var x1 = parseInt(document.getElementById('x1').value);
       var x2 = parseInt(document.getElementById('x2').value);
       if(Number.isNaN(x1) || Number.isNaN(x2)){
         alert("В поля x1 и x2 должны быть введены числовые значения");
       } else {
          var newVal;
          if (document.getElementById('sum').checked === true) {
            var sum = 0;
            for(var i = x1; i <= x2;i++){
              sum += i;
            }
            newVal = sum;
          }
          if (document.getElementById('mul').checked === true) {
            var mult = 1;
            for(var i = x1; i <= x2;i++){
              mult *= i;
            }
            newVal = mult;
          }
          if (document.getElementById('simple').checked === true) {
            var num = [];
            for(var i = 2; i <= x2;i++){
              num.push(i);
            }
            num.forEach((selected,sIndex,arr) => {
              if(selected !== -1){
                arr.forEach((val,vIndex,arr) =>{
                  if((val !== -1) && (val !== selected) && (val % selected === 0)){
                    arr[vIndex] = -1;
                  }
                })
              }
            });
            newVal = num.filter((item) =>{
              return item > 0;
            });
            newVal = newVal.filter((item) =>{
              return item >= x1;
            });
          }
          var resultDiv = document.getElementById('result');
          resultDiv.append("result is: "+(newVal));
          document.getElementById('x1').value = '';
          document.getElementById('x2').value = '';
       }
    }
}

function onClearClick() {
  document.getElementById('x1').value = '';
  document.getElementById('x2').value = '';
}
